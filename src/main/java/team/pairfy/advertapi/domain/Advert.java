package team.pairfy.advertapi.domain;

import lombok.Data;

@Data
public class Advert {
    private String id;
    private String text;
}
