package team.pairfy.advertapi.infra.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import team.pairfy.advertapi.application.query.GetAdvertBYIdQuery;
import team.pairfy.advertapi.application.query.GetAdvertBYIdQueryHandler;
import team.pairfy.advertapi.domain.Advert;

@RestController
public class AdvertController {

    @Autowired
    GetAdvertBYIdQueryHandler getAdvertBYIdQueryHandler;

    @GetMapping("id/{id}")
    public Advert id(@PathVariable String id) {
        return getAdvertBYIdQueryHandler.handle(new GetAdvertBYIdQuery(id));
    }


}
