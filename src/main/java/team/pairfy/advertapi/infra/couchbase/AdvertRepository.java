package team.pairfy.advertapi.infra.couchbase;

import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.json.JsonObject;
import com.couchbase.client.java.kv.GetResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import team.pairfy.advertapi.domain.Advert;

@Component
public class AdvertRepository {

    @Autowired
    Bucket advertBucket;

    public Advert load(String id)
    {
        GetResult getResult = advertBucket.defaultCollection().get(id);
        return getResultToAdvert(getResult);
    }

    private Advert getResultToAdvert(GetResult getResult) {
        Advert advert = new Advert();

        JsonObject json = getResult.contentAsObject();

        String id = (String) json.get("id");

        advert.setId(id);


        return advert;
    }


}
